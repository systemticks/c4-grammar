import { assert } from "chai";
import { WorkspaceConfiguration } from "vscode";
import { ConfigurationService, PreviewRenderer } from "../../../services/Configuration";

suite("ConfigurationService Test Suite", () => {
    test("getInlineRenderer should return the correct renderer", () => {
        const mockConfig = {
            get: <T>(key: string, defaultValue?: T): T => {
                if (key === "c4.diagram.renderer") {
                    return "plantuml" as T;
                }
                return defaultValue as T;
            },
        } as unknown as WorkspaceConfiguration;
        
        const configService = new ConfigurationService(mockConfig);
        const result = configService.getInlineRenderer();
        
        assert.strictEqual(result, "plantuml" as PreviewRenderer);
    });
});
