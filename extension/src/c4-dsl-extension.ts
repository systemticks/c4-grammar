// Copyright (c) 2020 systemticks GmbH
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

import {
  ExtensionContext,
  workspace,
  commands,
  window,
  StatusBarAlignment,
  Uri,
  TextDocument,
  TextEditorDecorationType,
} from "vscode";
import * as path from "path";
import * as cp from "child_process";
import * as findJavaHome from "find-java-home";

import {
  LanguageClientOptions,
  StateChangeEvent,
  State,
} from "vscode-languageclient";
import { LanguageClient } from "vscode-languageclient/node";
import {
  CommandResultCode,
  ConfigurationOptions,
  PlantUmlExportOptions,
} from "./types";
import { ConfigurationService, DecorationService, LanguageServer, PreviewService } from "./services";
import { C4Utils, substituteVariable } from "./utils";
import { PreviewRenderer } from "./services/Configuration";
import { logger, outputChannel } from "./utils/logger"

var structurizrPreviewService: PreviewService
var plantumlPreviewService: PreviewService
var mermaidPreviewService: PreviewService
var c4LanguageServer: LanguageServer

const config: ConfigurationService = new ConfigurationService(workspace.getConfiguration())

function getJavaPath() {
  let javaPath = config.getLanguageServerJava()

  // Check if a path is provided, if not, use JAVA_HOME or find a default
  if (!javaPath || javaPath.trim() === "") {
    javaPath = process.env.JAVA_HOME ?? "";
    if (!javaPath) {
      findJavaHome((err, home) => {
        if (err) {
          logger.error("Java home not found automatically.");
          return;
        }
        javaPath = home;
      });
      logger.info(
        "No custom JDK path set. Attempting to use JAVA_HOME or system default."
      );
    }
  } else if (javaPath.includes("${")) {
    javaPath = parseDir(javaPath);
  }

  return javaPath;
}

export function activate(context: ExtensionContext) {
  const javaPath = getJavaPath();
  logger.info("Java Path: {}", javaPath)

  const javaBinPath = path.join(javaPath, "bin");
  const env = {
    ...process.env,
    PATH: `${javaBinPath}${path.delimiter}${process.env.PATH}`,
  };

  cp.exec("java -version", { env }, (err, stdOut, stdErr) => {
    if (
      err?.message.includes("'java' is not recognized") ||
      err?.message.includes("'java' not found")
    ) {
      window.showErrorMessage(
        "Java is needed to run the Language server. Please install java"
      );
      logger.error("No Java installed")
    } else if (C4Utils.getJavaVersion(stdErr) < 17) {
      window.showErrorMessage(
        "Java 17 or higher is needed to run the Language server. Please upgrade your java version"
      );
      logger.error("Java 17 or higher required")
    } else {
      initExtension(context);
    }
  });
}

function handleTextDecorations(decType: TextEditorDecorationType) {

  const textDecorations = config.getTextDecorations()

  if (textDecorations !== "off") {
    const decorationService = new DecorationService(decType);

    if (textDecorations === "onSave") {
      workspace.onDidSaveTextDocument((savedDocument) => {
        decorationService.triggerDecorations(undefined, savedDocument);
      });
    } else if (textDecorations === "onChange") {
      workspace.onDidChangeTextDocument((changed) => {
        decorationService.triggerDecorations(undefined, changed.document);
      });
    }

    window.onDidChangeActiveTextEditor((editor) => {
      decorationService.triggerDecorations(editor, undefined);
    });

    decorationService.triggerDecorations(window.activeTextEditor, undefined);
  }
}

function registerPreviewService(previewService: PreviewService, commandId: string) {

  commands.registerCommand(commandId, async (...args: string[]) => {
      const encodedView = args[0];
      const diagramKey = args[1];
      previewService.currentDiagram = diagramKey;
      previewService.currentDocument = window.activeTextEditor?.document as TextDocument;

      return previewService.updateWebView(encodedView);
    }
  );
}

function setupPreviewServices() {
  
  structurizrPreviewService = new PreviewService(
    "https://structurizr.com/json",
    "Structurizr Preview",
    "Structurizr Preview"
  );

  if(config.isDiagramStructurizrEnabled()) {
    try {
      registerPreviewService(structurizrPreviewService, "c4.show.diagram")
    } catch (err) {
      logger.error("Error displaying preview: " + JSON.stringify(err));
    }
  }
  else {
    window.showInformationMessage(
      "You have to set the config item 'c4.diagram.structurizr.enabled' to true, if you want to use the public structurizr renderer"
    );
  }

  const plantumlRendererUri = config.getPlantUmlServer()
  plantumlPreviewService = new PreviewService(
    plantumlRendererUri.concat("/plantuml/svg/"),
    "UML",
    "PlantUML Preview"
  );

  
  if(config.isDiagramPlantUmlEnabled()) {
    try {
      registerPreviewService(plantumlPreviewService, "c4.show.plantuml")
    } catch (err) {
      logger.error("Error displaying preview: " + JSON.stringify(err));
    }
  }
  else {
    window.showInformationMessage(
      "You have to set the config item 'c4.diagram.plantuml.enabled' to true, if you want to use the public kroki rendering service"
    );
  }

  mermaidPreviewService = new PreviewService(
    "https://mermaid.ink/svg/",
    "UML",
    "Mermaid Preview"
  );

  if(config.isDiagramMermaidEnabled()) {
    try {
      registerPreviewService(mermaidPreviewService, "c4.show.mermaid")
    } catch (err) {
      logger.error("Error displaying preview: " + JSON.stringify(err));
    }
  }
  else {
    window.showInformationMessage(
      "You have to set the config item 'c4.diagram.mermaid.enabled' to true, if you want to use the public mermaid rendering service"
    );
  }

}

function onSaveDocument(renderer: PreviewRenderer) {
  
  workspace.onDidSaveTextDocument((document: TextDocument) => {
    switch (renderer) {
      case "structurizr":
        structurizrPreviewService.triggerRefresh(document, renderer);
        break;
      case "plantuml":
        plantumlPreviewService.triggerRefresh(document, renderer);
        break;
      case "mermaid":
        mermaidPreviewService.triggerRefresh(document, renderer);
        break;
      default:
        plantumlPreviewService.triggerRefresh(document, renderer);
        break;
    }
  });

}

function setupLanguageClient(): LanguageClient {

  const clientOptions: LanguageClientOptions = {
    documentSelector: [{ scheme: "file", language: "c4" }],
    outputChannel: outputChannel,
    synchronize: {
      fileEvents: workspace.createFileSystemWatcher("**/*.dsl"),
    },
  };

  return new LanguageClient(
    "c4LanguageClient",
    "C4 Language Server",
    c4LanguageServer.getServerOptions(),
    clientOptions
  );

}

function initExtension(context: ExtensionContext) {

  logger.info("Initialize extension")
  logger.info(config.dumpConfiguration())

  const connectionType = config.getLanguageServerConnectionType()
  const renderer = config.getInlineRenderer()    

  logger.info("Create C4 Language Server")
  c4LanguageServer = new LanguageServer(context, connectionType, renderer);

  logger.info("Create C4 Language Client")
  const c4LanguageClient = setupLanguageClient();

  const statusBarItem = window.createStatusBarItem(
    StatusBarAlignment.Right,
    100
  );
  statusBarItem.show();
  context.subscriptions.push(statusBarItem);

  c4LanguageClient.onDidChangeState((e: StateChangeEvent) => {
    switch (e.newState) {
      case State.Starting:
        statusBarItem.text = "C4 DSL Language Server is starting up...";
        statusBarItem.color = "white";
        break;
      case State.Running:
        statusBarItem.text = "C4 DSL Language Server is ready";
        statusBarItem.color = "white"
        updateServerConfigurationIndent();
        break;
      case State.Stopped:
        statusBarItem.text = "C4 Language Server has stopped";
        statusBarItem.color = "red";
        break;
    }
  });

  if (c4LanguageServer.isSocketConnection()) {
    statusBarItem.text = "C4 DSL Socket Server is starting up...";
    statusBarItem.color = "white";

    c4LanguageServer.spawnLanguageServer(config.isLanguageServerLogsEnabled()).then( (isReady) => {
      if(isReady) {
        c4LanguageClient.start();
      }
      else {
        statusBarItem.text = "Connection to C4 DSL Socket Server could not be established";
        statusBarItem.color = "red";
        logger.error("Connection to C4 DSL Socket Server could not be established")
      }
    })

  } else {
    c4LanguageClient.start();
  }

  // Defined here and not in the decoration service, as the decorations were being appended multiple times
  handleTextDecorations(window.createTextEditorDecorationType({}))

  setupPreviewServices()

  commands.registerCommand("c4.export.puml", (uri: Uri) => {
    const renderer = config.getPlantUmlGenerator()
    const exportDir = getExportDir();

    const exportOptions: PlantUmlExportOptions = {
      uri: uri.path,
      renderer: renderer,
      outDir: exportDir,
    };

    commands
      .executeCommand("c4-server.export.puml", exportOptions)
      .then((callback) => {
        const result = callback as CommandResultCode;
        if (result.resultcode == 100) {
          window.showInformationMessage(result.message);
        } else {
          window.showErrorMessage(
            "(Code:" + result.resultcode + ") " + result.message
          );
        }
      });
  });

  onSaveDocument(renderer);

  workspace.onDidChangeConfiguration((event) => {
    if (event.affectsConfiguration('c4.editor.autoformat.indent')) {
      updateServerConfigurationIndent();
    }
  });

}

function getExportDir() {
  const exportDir = config.getPlantUmlExportDir()
  if (exportDir.includes("${")) return parseDir(exportDir);
  return exportDir;
}

function parseDir(dir: string) {
  const separator = substituteVariable("${/}") as string;
  const paths = dir.split(separator);
  const parsedPath: Array<string> = [];
  for (let path of paths) {
    const pathValue = substituteVariable(path);
    if (!pathValue) {
      window.showErrorMessage("Incorrect path");
      return "";
    }
    parsedPath.push(pathValue);
  }
  return parsedPath.join(separator);
}

function updateServerConfigurationIndent() {
  const spaces = config.getAutoFormatIndent()
  commands.executeCommand("c4-server.autoformat.indent", { indent: spaces });
}

export function updateServerConfiguration() {
  const configOptions: ConfigurationOptions = {
    renderer: config.getInlineRenderer()
  };

  commands
    .executeCommand("c4-server.configuration", configOptions)
    .then((callback) => {
      window.showInformationMessage("Configuration Updated");
    });
}

export function deactivate() {
  if (c4LanguageServer) {
    c4LanguageServer.shutdown()
  }
}
