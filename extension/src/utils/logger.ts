import { LogLevel } from "typescript-logging";
import { Log4TSProvider, Logger, Log4TSGroupConfigOptional } from "typescript-logging-log4ts-style";
import { window, OutputChannel } from "vscode";

// Create a VS Code OutputChannel
const outputChannel: OutputChannel = window.createOutputChannel("C4 DSL Extension");

// Define log groups (required in newer versions)
const logGroupConfig: Log4TSGroupConfigOptional = {
  expression: new RegExp(".*"), // Matches all loggers
  level: LogLevel.Info, // Default log level
};

// Configure the TypeScript Logging provider
const provider = Log4TSProvider.createProvider("C4ExtensionProvider", {
  groups: [logGroupConfig], // Include the required groups property
});

// Create a logger instance
const logger: Logger = provider.getLogger("C4ExtensionLogger");

// Override logger methods to also write to the VS Code OutputChannel
const logMethods = ["trace", "debug", "info", "warn", "error", "fatal"] as const;

logMethods.forEach((method) => {
  const originalMethod = logger[method].bind(logger);

  logger[method] = (message: string, ...args: any[]) => {
    // Log to TypeScript Logging system
    originalMethod(message, ...args);

    // Format message with timestamp
    const timestamp = new Date().toISOString();
    const formattedMessage = `[${timestamp}] [${method.toUpperCase()}] ${message}`;

    // Log to VS Code OutputChannel
    outputChannel.appendLine(formattedMessage);
  };
});

export { logger, outputChannel };
