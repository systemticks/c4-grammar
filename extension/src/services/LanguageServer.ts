import { ExtensionContext, workspace } from "vscode";
import { ConnectionType, PreviewRenderer } from "./Configuration";
import * as cp from "child_process";
import * as readline from "node:readline";
import * as path from "path";
import { ServerOptions, StreamInfo } from "vscode-languageclient/node";
import * as net from "net";

class LanguageServer {

    private proc: cp.ChildProcess;
    private serverLauncher: string;
    private connectionType: ConnectionType;
    private renderer: PreviewRenderer;
    
    constructor(context: ExtensionContext, connectionType: ConnectionType, renderer: PreviewRenderer) {
        const executable = process.platform === "win32" ? "c4-language-server.bat" : "c4-language-server";
        const languageServerPath = path.join("server", "c4-language-server", "bin", executable);
        this.serverLauncher = context.asAbsolutePath(languageServerPath);
        this.connectionType = connectionType;
        this.renderer = renderer;
    }

    getLauncher(context: ExtensionContext): string {
        return this.serverLauncher;
    }

    private isProcessIO(): boolean {
        return this.connectionType === "process-io" ||
            (this.connectionType === "auto" && process.platform === "win32")
    }

    isSocketConnection(): boolean {
        return this.connectionType === "socket" ||
            (this.connectionType === "auto" && process.platform !== "win32")
    }

    getServerOptions(): ServerOptions {
        if (this.isProcessIO()) {
          return {
            run: {
              command: this.serverLauncher,
              options: {shell: true},
              args: ["-ir=" + this.renderer],
            },
            debug: {
              command: this.serverLauncher,
              options: {shell: true},
              args: ["-ir=" + this.renderer],
            },
          };
        } else {
          const serverDebugOptions = () => {
            let socket = net.connect({ port: 5008 });
            let result: StreamInfo = {
              writer: socket,
              reader: socket,
            };
            return Promise.resolve(result);
          };
          return serverDebugOptions;
        }
      }

    async spawnLanguageServer(serverLogsEnabled: boolean): Promise<boolean> {

      const READY_ECHO = "READY_TO_CONNECT";
      const args = ["-c=socket", "-e=" + READY_ECHO, "-ir=" + this.renderer];
        
      if (serverLogsEnabled && workspace.workspaceFolders) {
        const wsFolder = workspace.workspaceFolders[0].uri;
        this.proc = cp.spawn(this.serverLauncher, args, { cwd: wsFolder.fsPath, shell: true });
      } else {
        this.proc = cp.spawn(this.serverLauncher, args, { shell: true });
      }
    
      return new Promise<boolean>((resolve) => {
        if (this.proc.stdout) {
          const reader = readline.createInterface({
            input: this.proc.stdout,
            terminal: false,
          });
    
          reader.on("line", (line: string) => {
            if (line.endsWith(READY_ECHO)) {
              resolve(true);
              reader.close();
            }
          });
    
        } else {
          resolve(false);
        }
      });
    }

    shutdown(): void {
        if (this.proc) {
            this.proc.kill("SIGINT");
        }
    }
}

export { LanguageServer }