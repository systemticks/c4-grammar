import { WorkspaceConfiguration } from "vscode";

type PreviewRenderer = "plantuml" | "structurizr" | "mermaid"
type ConnectionType = 'auto' | 'process-io' | 'socket' | 'socket-debug'
type TextDecoratioUpdateType = 'off' | 'onSave' | 'onChange'

class ConfigurationService {
    private static readonly CONFIG_KEYS = {
        INLINE_RENDERER: "c4.diagram.renderer",
        PLANTUML_GENERATOR: "c4.export.plantuml.generator",
        PLANTUML_EXPORT_DIR: "c4.export.plantuml.dir",
        LANGUAGESERVER_CONNECTIONTYPE: "c4.languageserver.connectiontype",
        DIAGRAM_STRUCTURIZR_ENABLED: "c4.diagram.structurizr.enabled",
        DIAGRAM_PLANTUML_ENABLED: "c4.diagram.plantuml.enabled",
        DIAGRAM_MERMAID_ENABLED: "c4.diagram.mermaid.enabled",
        PLANTUML_SERVER: "c4.show.plantuml.server",
        TEXT_DECORATIONS: "c4.decorations.enabled",
        LANGUAGESERVER_LOGS_ENABLED: "c4.languageserver.logs.enabled",
        AUTO_FORMAT_INDENT: "c4.editor.autoformat.indent",
        LANGUAGESERVER_JAVA: "c4.languageserver.java",
    } as const;

    private config: WorkspaceConfiguration;

    constructor(workspaceConfiguration: WorkspaceConfiguration) {
        this.config = workspaceConfiguration;
    }

    getConfigValue<T>(key: string, defaultValue?: T): T {
        return this.config.get<T>(key, defaultValue as T) as T;
    }

    getInlineRenderer(): PreviewRenderer {
        return this.getConfigValue<PreviewRenderer>(ConfigurationService.CONFIG_KEYS.INLINE_RENDERER);
    }

    getPlantUmlGenerator(): string {
        return this.getConfigValue<string>(ConfigurationService.CONFIG_KEYS.PLANTUML_GENERATOR);
    }

    getPlantUmlExportDir(): string {
        return this.getConfigValue<string>(ConfigurationService.CONFIG_KEYS.PLANTUML_EXPORT_DIR);
    }

    getLanguageServerConnectionType(): ConnectionType {
        return this.getConfigValue<ConnectionType>(ConfigurationService.CONFIG_KEYS.LANGUAGESERVER_CONNECTIONTYPE);
    }

    isDiagramStructurizrEnabled(): boolean {
        return this.getConfigValue<boolean>(ConfigurationService.CONFIG_KEYS.DIAGRAM_STRUCTURIZR_ENABLED, false);
    }

    isDiagramPlantUmlEnabled(): boolean {
        return this.getConfigValue<boolean>(ConfigurationService.CONFIG_KEYS.DIAGRAM_PLANTUML_ENABLED, false);
    }

    isDiagramMermaidEnabled(): boolean {
        return this.getConfigValue<boolean>(ConfigurationService.CONFIG_KEYS.DIAGRAM_MERMAID_ENABLED, false);
    }

    getPlantUmlServer(): string {
        return this.getConfigValue<string>(ConfigurationService.CONFIG_KEYS.PLANTUML_SERVER);
    }

    getTextDecorations(): TextDecoratioUpdateType {
        return this.getConfigValue<TextDecoratioUpdateType>(ConfigurationService.CONFIG_KEYS.TEXT_DECORATIONS);
    }

    isLanguageServerLogsEnabled(): boolean {
        return this.getConfigValue<boolean>(ConfigurationService.CONFIG_KEYS.LANGUAGESERVER_LOGS_ENABLED, false);
    }

    getAutoFormatIndent(): number {
        return this.getConfigValue<number>(ConfigurationService.CONFIG_KEYS.AUTO_FORMAT_INDENT, 4);
    }

    getLanguageServerJava(): string {
        return this.getConfigValue<string>(ConfigurationService.CONFIG_KEYS.LANGUAGESERVER_JAVA);
    }

    dumpConfiguration(): string {
        const configEntries = Object.entries(ConfigurationService.CONFIG_KEYS).map(([key, value]) => {
            const configValue = this.getConfigValue(value);
            return `${key}: ${JSON.stringify(configValue)}`;
        });
        return `Configuration Dump:\n${configEntries.join("\n")}`;
    }
}

export { ConfigurationService, PreviewRenderer, ConnectionType, TextDecoratioUpdateType };
